<?php
/**
 * Une page de démonstration vide
 * Le contenu de la variable $headerContent sera effecter a header.php pour lui frounir des ressources javascript et CSS
 * Le contenu de la variable $footerContent sera effecter a footer.php pour lui frounir des ressources javascript et CSS
 ***** NE PAS SUPPRIMER OU MODIFIER CE FICHIER ****
 */
?>

<?php
$menuActuel = "menu_etudiant";
$sousMenuActuel = "menu_etudiant_etudiant";
$headerContent = <<<EOF
EOF;
$footerContent = <<<EOF
<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#{$menuActuel}").trigger("click");
    jQuery(".nav-parent > a#{$menuActuel}").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#{$sousMenuActuel}").addClass("active");



  });
 </script>
EOF;
?>

<?php  include("layout/header.php"); ?>
<?php  include("layout/leftpanel.php"); ?>
<?php  include("layout/topmenu.php"); ?>

   <div class="pageheader">
      <h2><i class="fa fa-home"></i> liste des etudiants <span>Subtitle goes here...</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Bracket</a></li>
          <li class="active">Blank</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">

     
<div >
 
  <table class="table">
    <thead>
      <tr >
        <th>Nom</th>
        <th>Prenom</th>
        <th>Email</th>
        <th>Centre</th>
        <th>Telephone</th>
      </tr>
    </thead>
    <tbody>
      <tr class="success">
        <td>Hamza</td>
        <td>Amri</td>
        <td>hamza.amri@gmail.com</td>
        <td>El jadida</td>
        <td>06 02368945</td>
      </tr>
      <tr class="danger">
       <td>Saad</td>
        <td>Hasbi</td>
        <td>hasbi.saad@gmail.com</td>
        <td>casa blanca</td>
                <td>06 22368945</td>

      </tr>
      <tr class="info">
      <td>Marmoucha</td>
        <td>Ossama</td>
        <td>Ossama.marmoucha@gmail.com</td>
        <td>Fes</td>
                <td>06 12368945</td>

      </tr>
    </tbody>
  </table>
</div>

    </div>

<?php  include("layout/rightpanel.php"); ?>
<?php  include("layout/footer.php"); ?>