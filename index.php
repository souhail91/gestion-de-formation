<?php
/**
 * Une page de démonstration vide
 * Le contenu de la variable $headerContent sera effecter a header.php pour lui frounir des ressources javascript et CSS
 * Le contenu de la variable $footerContent sera effecter a footer.php pour lui frounir des ressources javascript et CSS
 ***** NE PAS SUPPRIMER OU MODIFIER CE FICHIER ****
 */
?>

<?php
$menuActuel = "menu_dashboard";


$headerContent = <<<EOF
EOF;
$footerContent = <<<EOF
<script src="js/jquery.mousewheel.js"></script>
<script src="js/chosen.jquery.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script>
  jQuery(document).ready(function() {

    jQuery("a#{$menuActuel}").parent('li').addClass("active");


  });
</script>
EOF;
?>

<?php  include("layout/header.php"); ?>
<?php  include("layout/leftpanel.php"); ?>
<?php  include("layout/topmenu.php"); ?>

   <div class="pageheader">
      <h2><i class="fa fa-home"></i> Tableau de bord</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li class="active">Dashboard</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      <p>Hello World!!</p>
    </div>

<?php  include("layout/rightpanel.php"); ?>
<?php  include("layout/footer.php"); ?>